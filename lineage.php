<?php
$json = file_get_contents("https://raw.githubusercontent.com/LineageOS/hudson/main/updater/devices.json");
#$json = file_get_contents("devices.json");

$supported = array();

$targets = file_get_contents("https://raw.githubusercontent.com/LineageOS/hudson/main/lineage-build-targets");
$targets = explode("\n",$targets);
foreach ($targets as $target)
{
    if (!$target) continue;
    if (strpos($target,0,1) == "#") continue;
    $array = explode(" ", $target);
    $release = $array[2];
    $device = $array[0];
    if (!isset($supported[$release]))
    {
        $supported[$release] = array();
    }
    $supported[$release][] = $device;
}

$devices = json_decode($json);

usort($devices, function($a, $b) {
    $cmp = strcmp($a->oem, $b->oem);
    if ($cmp == 0) {
        return strcmp($a->name,$b->name);
    }
    return $cmp;
});

?>
<!DOCTYPE html>

<meta name="viewport" content="width=device-width, initial-scale=1.0"> 

<style>
  tr:nth-child(even) {background-color: #f2f2f2;}
  tr:nth-child(odd)  {background-color: white;}
</style>

<table>
<?php
foreach($devices as $device)
{
    $supported_release = false;
    foreach ($supported as $release=>$supported_devices)
    {
        if (in_array($device->model,$supported_devices))
        {
            $supported_release = $release;
            break;
        }
    }
    if ($supported_release)
    {
        echo "<tr>";
        echo "<td>" . $device->oem  . "</td>";
        echo "<td>" . $device->name . "</td>";
        echo "<td><a href='https://download.lineageos.org/" . $device->model . "'>" . $supported_release . "</a></td>";
        echo "<td>";
#       if ($supported_release == "lineage-16.0")
        {
            echo "<a href='https://download.lineage.microg.org/" . $device->model . "'>" . $supported_release . " (microg)</a>";
        }
        echo "</td>";
        echo "</tr>\n";
    }
}
?>
</table>
